import logging,traceback
import ddt
from selenium import  webdriver
import unittest,time
from selenium.common.exceptions import  NoSuchElementException
import  HTMLTestRunner
from mylesson.demo.mysqlUtil import MyMySql

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                    datefmt='%Y %m %d %H:%M:%S',
                    filename='D:\\study\\autotestddt\\report.log',
                    filemode='w',
                    )

def getTestData():
    db = MyMySql()
    execute_sql = "select 电影名,导演 from testdata"
    testData = db.getDataFromDataBases(execute_sql)
    db.closeDatabase()
    return  testData

@ddt.ddt
class TestDemo(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()

    @ddt.data(* getTestData())
    def test_dataDriverByDatabase(self,data):
        testdata,expectdata = data
        url = "https://www.baidu.com"
        self.driver.get(url)
        self.driver.implicitly_wait(10)
        self.driver.find_element_by_id("kw").send_keys(testdata)
        self.driver.find_element_by_id("su").click()
        time.sleep(3)
        if expectdata in self.driver.page_source:
            logging.info("搜索 {} 期望 {}，通过".format(testdata, expectdata))
            self.assertTrue(expectdata in self.driver.page_source)
        else:
            logging.info("搜索 {} 期望 {}，失败".format(testdata, expectdata))
            self.assertTrue(expectdata in self.driver.page_source)

    def tearDown(self):
        self.driver.quit()
if __name__ == '__main__':
    unittest.main()
