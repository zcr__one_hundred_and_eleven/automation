import logging,traceback
import ddt
from selenium import  webdriver
import unittest,time
from selenium.common.exceptions import  NoSuchElementException
import  HTMLTestRunner


logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                    datefmt='%Y %m %d %H:%M:%S',
                    filename='D:\\study\\autotestddt\\report.log',
                    filemode='w',
                    )

@ddt.ddt
class TestDemo(unittest.TestCase):

    def setUp(self):
        self.driver=webdriver.Chrome()


    @ddt.file_data("test_data_list.json")

    def test_dataDriverByFile(self,value):
        url = "https://www.baidu.com"
        self.driver.get(url)
        print(value)
        testdata,expectdata = tuple(value.strip().split(':'))
        self.driver.implicitly_wait(10)
        # try:
        self.driver.find_element_by_id("kw").send_keys(testdata)
        self.driver.find_element_by_id("su").click()
        time.sleep(3)
        if expectdata in self.driver.page_source:
            logging.info("搜索 {} 期望 {}，通过".format(testdata, expectdata))
            self.assertTrue(expectdata in self.driver.page_source)
        else:
            logging.info("搜索 {} 期望 {}，失败".format(testdata, expectdata))
            self.assertTrue(expectdata in self.driver.page_source)
            # except Exception as e:
        # except NoSuchElementException as e:
        #     logging.error("查找的页面元素不存在，异常堆栈信息:" + str(traceback.format_exc()))
        # except AssertionError as e:
        #     logging.info("搜索{} 期望{}，失败".format(testdata, expectdata))
        # except Exception as e:
        #     logging.info("未知错误，错误信息" + str(traceback.format_exc()))
        # else:
        #     logging.info("搜索{} 期望{}，通过".format(testdata, expectdata))

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    # unittest.main()
    suite1 = unittest.TestLoader().loadTestsFromTestCase(TestDemo)
    suite = unittest.TestSuite(suite1)
    # suite.addTest(TestDemo('test_dataDriverByFile'))
    filename = time.strftime("%Y%m%d%H%M")
    filepath = filename + "result.html"
    result = open(filepath, 'wb')
    runner = HTMLTestRunner.HTMLTestRunner(stream=result, title='测试报告', description='测试执行情况')
    runner.run(suite)
    result.close()