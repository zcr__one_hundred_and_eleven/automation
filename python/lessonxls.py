import xlrd, xlwt   #xls
import openpyxl    #xlsx
from xlutils.copy import copy

'''
xlrd 读xls文件
'''
# workbook = xlrd.open_workbook('interface_api.xls')
# sheet = workbook.sheet_by_index(0)
# print(sheet)
# cell = sheet.cell(2,2)
# print(type(cell))
# print(cell)
# vaule = sheet.cell_value(2,2)
# print(type(vaule))
# print(vaule)
# print(sheet.row(1))
# print(sheet.nrows)
# for i in range(sheet.nrows):
#     print(sheet.row(i))

# for i in range(sheet.nrows):
#     for j in range(sheet.ncols):
#         print(sheet.cell_value(i,j),end=' ')
#     print()

#
# names = workbook.sheet_names()
# print(names)
# for name in names:
#     sheet = workbook.sheet_by_name(name)
#     for i in range(sheet.nrows):
#         for j in range(sheet.ncols):
#             print(sheet.cell_value(i,j),end=' ')
#         print()

# '''
# xlwt 写xls文件
# '''
# newbook = xlwt.Workbook()
# newsheet = newbook.add_sheet('testsheet1')
# newsheet.write(2,2,'hello')
# newbook.save('newxls.xls')
# #
# '''
# 追加写入
# '''
# read_data = xlrd.open_workbook('newxls.xls')
# write_date = copy(read_data)
# sheet = write_date.get_sheet(0)
# sheet.write(1,1,'hello1')
# write_date .save('addxls.xls')

#openyxl
wb = openpyxl.load_workbook("126邮箱联系人.xlsx")
sheet_names = wb.sheetnames
print(sheet_names)
sheet = wb.get_sheet_by_name("联系人")
# sheet = wb["联系人"]
# print(sheet.max_row)
# print(sheet.max_column)
# print(sheet.cell(1,1))
# print(type(sheet.cell(1,1)))
# print(sheet.cell(1,1).value)
# for row in sheet.rows:
#     print(row)
#     for cell in row:
#         print(cell.value,end=' ')
#     print()

#
# wb = openpyxl.Workbook()
# wb.create_sheet("test1")
# wb.save("test123.xlsx")

