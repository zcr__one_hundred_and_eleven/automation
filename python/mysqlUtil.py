import pymysql

class MyMySql:
    def __init__(self,host="127.0.0.1",port=3306,user="root",password = "password",db="pythontest"):
        self.host= host
        self.port = port
        self.user = user
        self.password = password
        self.db = db

        self.conn = pymysql.connect(host=self.host,port=self.port,user=self.user,
                                    password=self.password,db=self.db)
        self.cur = self.conn.cursor()

    def getDataFromDataBases(self,sql):
        self.cur.execute(sql)
        dataTuple =  self.cur.fetchall()
        return  dataTuple

    def closeDatabase(self):
        self.conn.close()

if __name__ == '__main__':
    db = MyMySql()
    execute_sql = "select 电影名,导演 from testdata;"
    res = db.getDataFromDataBases(execute_sql)
    print(res)